<?php 

$dataID         = $data[0];
$status         = $data[1];
$title          = $data[2];
$dates          = $data[3];
$blurb          = $data[4];

$college        = $data[5];
$summer         = $data[6];
$overnight      = $data[7];
$scholarship    = $data[8];

$program        = $data[9];
$place          = $data[10];
$resource       = $data[11];

$gpre_k         = $data[12];
$gk_2           = $data[13];
$g3_5           = $data[14];
$g6_8           = $data[15];
$g9_12          = $data[16];

$topic_ag       = $data[17];
$topic_an       = $data[18];
$topic_art      = $data[19];
$topic_bus      = $data[20];
$topic_com      = $data[21];
$topic_eng      = $data[22];
$topic_led      = $data[23];
$topic_mat      = $data[24];
$topic_soc      = $data[25];
$topic_spr      = $data[26];
$topic_wrt      = $data[27];
$topic_cp       = $data[28];
$topic_gen      = $data[29];
$topic_env      = $data[30];
$topic_med      = $data[31];

$resourceImg    = $data[32];

?>