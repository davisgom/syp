<section class="grade-header">
    <div class="container">
   
      <p>For grades:</p>

      <ul class="grade-header-list">
        <li> <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">Pre-K</a>  </li>
        <li> <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">K-2</a>  </li>
        <li> <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">3-5</a>  </li>
        <li> <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">6-8</a>  </li>
        <li> <a href="#" class="btn btn-theme-outline btn-theme-outline-reversed">9-12</a>  </li>
      </ul>

    </div>
  </section>