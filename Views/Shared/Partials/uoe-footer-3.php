<footer class="uoe-footer uoe-footer-dark text-center">
	<div class="container">
		<div class="row">
			<div class="col-12">		
				<p>
					<span class="d-inline-block" style="font-size: 85%; margin-bottom: -8px;">A program of</span>

					<a href="//engage.msu.edu" target="_blank" class="d-inline-block">
					<i class="icon" aria-hidden="true" style="position: relative; top: -2px; margin-left: 4px; display: inline-block; rotate: 1.5deg;">
						<svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 20.7 24"> <path fill="currentColor" d="M7.7,9.6l-1.7-2c-.4.3-.5.5-.7.8l2,1.6s.4-.4.4-.4ZM8.7,9.1l-1.3-2.3c-.5.2-.8.5-1,.6l1.7,2.1s.5-.3.5-.3ZM4.4,9.8l2.2,1.1s.3-.4.4-.4l-2-1.5c-.2.3-.4.6-.5.9ZM7,6c3.4-2,7.5-1.8,10.6-.4.6-1.3,1.9-4,1.9-4C11.2-2.8,2.9,2.6,1,7c-2.1,4.9,0,8.8.3,10.4.7,4-1.1,4.9-1.3,5.2,1.1,0,1.8-.9,2.4-1.8.4-.8.7-1.8.7-2.7.3-1.8-.5-3.7-.5-5.5.2-2.7,2-5.1,4.3-6.5ZM11.5,5.4v2.9s.2,0,.2,0c0-.1.1-1.1,1-1.9.9-.8,2.2-.7,2.2-.7-1.2-.4-2.5-.4-3.4-.3ZM3.7,11.3l2.2.4c0-.2.1-.3.2-.5l-2.2-.8c-.1.3-.2.7-.3.9ZM9.7,8.6l-.7-2.6c-.4.1-1,.4-1.1.4l1.2,2.4c.6-.2.6-.2.6-.2ZM3.5,14.8s.2-1,.8-1.6c.5-.5,1.1-.6,1.4-.6,0,0,.1-.4,0-.4l-2.1-.3c-.2,1.2-.2,2.3,0,2.9ZM17.9,13c-.8-1.9-2.4-3.2-4.5-3.6-1.1-.3-2.3-.2-3.4.1-1.6.6-2.6,1.7-3.3,3.2-1.1,2.6.5,4.5.5,6.6,0,.8-.4,2.3-.3,2.3,2.1-.3,3.4-.9,5.2-1.3,1.2,1.1,5.3,3.5,6.2,3.6,0,0-1-3.6-.8-7.6-1,.2-2.4-.3-3.2-1.1,1-1.5,4.8-1.3,4.3,2.7,0,0,2.2.4,2.2.4-1.4-1.7-2-3.4-2.8-5.4ZM10.8,8.4v-2.9s-.6.1-1.1.3l.6,2.7s.6-.1.6-.1Z"/></svg>
          			</i>

					<span class="text-uppercase" style="font-weight: 900;" >
						University Outreach and Engagement
					</span>

					</a>
				</p>
			</div>
    	</div>
	</div>
</footer>