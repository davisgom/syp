<section class="filter-options collapse" id="filter-options">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 <?php if($page_content == "search-by-grade"){echo "d-none";}  ?>">
                <h2>GRADES</h2>
                
                    <?php
                        foreach ($grades as $grade) {
                            echo '
                            
                            <input type="checkbox" class="btn-check" id="'.$grade.'" autocomplete="off" ></input>
                            <label class="btn btn-theme btn-theme-grade" for="'.$grade.'"> '.$grade.' </label>
                            
                            ';
                        }
                    ?>
                
            </div>

            <div class="col-md-6 <?php if($page_content == "search-by-topic"){echo "d-none";}  ?>">
                <h2>TOPICS</h2>

                <?php
                        foreach ($topics as $topic) {
                            echo '
                            
                            <input type="checkbox" class="btn-check" id="'.$topic.'" autocomplete="off" ></input>
                            <label class="btn btn-theme btn-theme-topic" for="'.$topic.'"> '.$topic.' </label>
                            
                            ';
                        }
                    ?>

            </div>

            <div class="col-lg-3">
                <div>
                    <h2>FEATURES</h2>

                    <?php
                            foreach ($features_var as $feature) {
                                echo '
                                
                                <input type="checkbox" class="btn-check" id='.$feature.' autocomplete="off" ></input>
                                <label class="btn btn-theme btn-theme-features" for='.$feature.'> '.$feature.' </label>
                                
                                ';
                            }
                    ?>
                </div>                

                <br />
                <br />

                <div class="date-input">   
                        <h2>DATES</h2>

                        <input class="text-box single-line col-5 select-date" id="StartDate" name="StartDate" type="date" value="" data-bs-toggle="tooltip" data-bs-title="Enter Start Date">
                        -
                        <input class="text-box single-line col-5 select-date" id="StopDate" name="StopDate" type="date" value="" data-bs-toggle="tooltip" data-bs-title="Enter End Date">
                </div>
            </div>

            <div>
                
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12 col-sm-2">
                <button class="btn btn-theme btn-theme-syp-link d-block text-center p-2" type="button" data-bs-toggle="collapse" data-bs-target="#filter-options" aria-expanded="false" aria-controls="filter-options">
                    Apply Filters
                    </button>
            </div>
        </div>
    </div>
</section>