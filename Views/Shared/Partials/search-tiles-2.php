  <div class="container">
    <div class="row">
    <div class="col-11 col-md syp-search-feature homepage-places">
        <h2>
            Places <br class="d-xl-none" /> to Visit 

            <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 1.5deg;">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="30" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M429.6 92.1c4.9-11.9 2.1-25.6-7-34.7s-22.8-11.9-34.7-7l-352 144c-14.2 5.8-22.2 20.8-19.3 35.8s16.1 25.8 31.4 25.8H224V432c0 15.3 10.8 28.4 25.8 31.4s30-5.1 35.8-19.3l144-352z"/></svg>
            </i>
          </h2>

          <p>
            Browse venues that offer learning opportunities for <span class="text-nowrap">pre-kindergarten</span> to high school learners.
          </p>

          <a href="places-to-visit" class="btn btn-theme btn-theme-syp-green btn-theme-small m-1">
            View Places
          </a>
      </div>

      <div class="col-11 col-md syp-search-feature homepage-summer">
        <h2>
          Summer <br class="d-xl-none" /> Programs
          
          <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 1.5deg;">
          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="30" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M346.3 271.8l-60.1-21.9L214 448H32c-17.7 0-32 14.3-32 32s14.3 32 32 32H544c17.7 0 32-14.3 32-32s-14.3-32-32-32H282.1l64.1-176.2zm121.1-.2l-3.3 9.1 67.7 24.6c18.1 6.6 38-4.2 39.6-23.4c6.5-78.5-23.9-155.5-80.8-208.5c2 8 3.2 16.3 3.4 24.8l.2 6c1.8 57-7.3 113.8-26.8 167.4zM462 99.1c-1.1-34.4-22.5-64.8-54.4-77.4c-.9-.4-1.9-.7-2.8-1.1c-33-11.7-69.8-2.4-93.1 23.8l-4 4.5C272.4 88.3 245 134.2 226.8 184l-3.3 9.1L434 269.7l3.3-9.1c18.1-49.8 26.6-102.5 24.9-155.5l-.2-6zM107.2 112.9c-11.1 15.7-2.8 36.8 15.3 43.4l71 25.8 3.3-9.1c19.5-53.6 49.1-103 87.1-145.5l4-4.5c6.2-6.9 13.1-13 20.5-18.2c-79.6 2.5-154.7 42.2-201.2 108z"/></svg>
          </i>
        </h2>

        <p>
          Browse programs available for pre-kindergarten to high school learners during the summer.
        </p>

        <a href="summer-programs" class="btn btn-theme btn-theme-syp-orange btn-theme-small m-1">
          View Programs
        </a>
      </div>
    </div>
  </div>