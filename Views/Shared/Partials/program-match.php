<h2>Find a Program</h2>

<form action="welcome_get.php" method="GET">
<div class="program-match-form row">     
  <div class="col-12 col-lg-3 mb-3 grade-input">
    <!-- <label class="col-12" for="grades">I'm looking for programs for</label> -->
    <select class="select-grades col-12" name="grades" id="grades">
      <option selected>Choose a grade</option>
        <?php
          foreach ($grades as $grade) {
            echo '
              <option value="'.$grade.'">
                
                  <span class="btn btn-outline-tag-grade">
                    '.$grade.' <!--Student(s)-->
                  </span>
              
            </option>
            ';
          }
        ?>
    </select>
  </div>
    
  <div class="col-12 col-lg-3 mb-3 topic-input">
    <!-- <label class="col-12" for="topics">who are interested in</label> -->
    <select class="select-topics col-12" name="topics" id="topics">
      
      <option selected>Choose a topic</option>
      <?php
            foreach ($topics as $topic) {
              echo '
                <option value="'.$topic.'">
                  
                    <span class="btn btn-outline-tag-grade">
                      '.$topic.'
                    </span>
                
              </option>
              ';
            }
          ?>
    </select>
  </div>

  <div class="col-12 col-lg-auto mb-3 date-input">
    <!-- <label class="col-12" for="topics">between the dates of</label> -->
    
      
    <div class="row justify-content-center">
      <input class="text-box single-line col-5 select-date" id="StartDate" name="StartDate" type="date" value="" data-bs-toggle="tooltip" data-bs-title="Enter Start Date">
      -
      <input class="text-box single-line col-5 select-date" id="StopDate" name="StopDate" type="date" value="" data-bs-toggle="tooltip" data-bs-title="Enter End Date">
    </div>
  </div>
  
  
  <div class="col-auto submit-input">
    <!-- <br /> -->
    <a href="search" class="btn btn-theme btn-theme-syp-link btn-theme-small submit-btn">
      Search
    </a>
  </div>
</div>


<div class="row">
  <div class="col-12" style="position: relative; top: 20px;">
    Or you  can <a href="search"><strong class="fw-800">browse all programs</strong></a>.
  </div>
</div>
</form>
  