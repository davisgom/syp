  <div class="container">
    <div class="row">
      <div class="col-11 col-md syp-search-feature homepage-grade">
        <h2>
          Search <br /> by Grade

          <i class="icon" aria-hidden="true" style="position: relative; top: -4px; margin-right: 4px; display: inline-block;">
            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="30" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M337.8 5.4C327-1.8 313-1.8 302.2 5.4L166.3 96H48C21.5 96 0 117.5 0 144V464c0 26.5 21.5 48 48 48H256V416c0-35.3 28.7-64 64-64s64 28.7 64 64v96H592c26.5 0 48-21.5 48-48V144c0-26.5-21.5-48-48-48H473.7L337.8 5.4zM96 192h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V208c0-8.8 7.2-16 16-16zm400 16c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H512c-8.8 0-16-7.2-16-16V208zM96 320h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V336c0-8.8 7.2-16 16-16zm400 16c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H512c-8.8 0-16-7.2-16-16V336zM232 176a88 88 0 1 1 176 0 88 88 0 1 1 -176 0zm88-48c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H336V144c0-8.8-7.2-16-16-16z"/></svg>
          </i>
        </h2>

        <p>
          Browse programs available to serve all age ranges from <span class="text-nowrap">pre-kindergarten</span> to high school.
        </p>

        <a href="search-by-grade" class="btn btn-theme btn-theme-syp-blue btn-theme-small m-1">
          Search by Grade
        </a>
      </div>

      <div class="col-11 col-md syp-search-feature homepage-topic">
          <h2>
            Search <br /> by Topic

            <i class="icon" aria-hidden="true" style="position: relative; top: -4px; margin-right: 4px; display: inline-block;">
              <svg xmlns="http://www.w3.org/2000/svg" width="48" height="30" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M272 384c9.6-31.9 29.5-59.1 49.2-86.2l0 0c5.2-7.1 10.4-14.2 15.4-21.4c19.8-28.5 31.4-63 31.4-100.3C368 78.8 289.2 0 192 0S16 78.8 16 176c0 37.3 11.6 71.9 31.4 100.3c5 7.2 10.2 14.3 15.4 21.4l0 0c19.8 27.1 39.7 54.4 49.2 86.2H272zM192 512c44.2 0 80-35.8 80-80V416H112v16c0 44.2 35.8 80 80 80zM112 176c0 8.8-7.2 16-16 16s-16-7.2-16-16c0-61.9 50.1-112 112-112c8.8 0 16 7.2 16 16s-7.2 16-16 16c-44.2 0-80 35.8-80 80z"/></svg>
            </i>
          </h2>

          <p>
            Browse program topics like art, business, engineering, math, music, science, and sports.
          </p>

          <a href="search-by-topic" class="btn btn-theme btn-theme-syp-red btn-theme-small m-1">
            Search by Topic
          </a>
      </div>

      <div class="col-11 col-md syp-search-feature homepage-places">
        <h2>
          Places <br /> to Visit 
          
          <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 1.5deg;">
            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="30" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M429.6 92.1c4.9-11.9 2.1-25.6-7-34.7s-22.8-11.9-34.7-7l-352 144c-14.2 5.8-22.2 20.8-19.3 35.8s16.1 25.8 31.4 25.8H224V432c0 15.3 10.8 28.4 25.8 31.4s30-5.1 35.8-19.3l144-352z"/></svg>
          </i>
        </h2>

          <p>
            Browse venues that offer learning opportunities for <span class="text-nowrap">pre-kindergarten</span> to high school learners
          </p>

          <a href="places-to-visit" class="btn btn-theme btn-theme-syp-green btn-theme-small m-1">
            View Places
          </a>
      </div>
    </div>
  </div>