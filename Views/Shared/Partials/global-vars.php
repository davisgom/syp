<?php
$site_title = "Spartan Youth Programs";

$keywords = "outreach engagement scholar scholarship community development responsive build capacity building collaborative collaboration michigan state university";

$author = "University Outreach and Engagement - Communication and Information Technology";

$description = "Michigan State University's Spartan Youth Programs Web site displays a wide range of exciting opportunities for youth to improve their knowledge and skills in specific subject areas. Programs are available to serve all age ranges from pre-kindergarten to high school.";

$main_phone = "(517) 353-8977";

$external = 'target="_blank" rel="noopener" data-toggle="tooltip" data-placement="bottom" title="Link opens in new window"';

$syp__color = array(
    "blue",
    "green",
    "yellow",
    "orange",
    "red",
    "purple", 
);

$cname = array(
    "Will",
    "Willis",
    "William",
    "Willum",
    "Willie",
    "Willa",
    "Willemina",
    "Wilma",
);

$grade__groups = array(
    "Pre-K",
    "K-2",
    "3-5",
    "6-8",
    "9-12", 
);

$grades = array(
    "Pre-K",
    "Kindergarten",
    "1st Grade",
    "2nd Grade",
    "3rd Grade",
    "4th Grade",
    "5th Grade",
    "6th Grade",
    "7th Grade",
    "8th Grade",
    "9th Grade",
    "10th Grade",
    "11th Grade",
    "12th Grade",
  );

$topics = array(
    "Agriculture",
    "Animal Care",
    "Art, Drama, & Music",
    "Business",
    "College Preparation",
    "Computers & Technology",
    "Engineering",
    "Environtmental Sciences & Natural Resources",
    "Leadership Skills",
    "Math & Science",
    "Medical Sciences",
    "Social Studies",
    "Sports, Fitness, & Nutrition",
    "Writing & Language",
);

$features = array(
    '
    <i class="icon" aria-hidden="true" style="position: relative; top: -1px;">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="15" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M320 32c-8.1 0-16.1 1.4-23.7 4.1L15.8 137.4C6.3 140.9 0 149.9 0 160s6.3 19.1 15.8 22.6l57.9 20.9C57.3 229.3 48 259.8 48 291.9v28.1c0 28.4-10.8 57.7-22.3 80.8c-6.5 13-13.9 25.8-22.5 37.6C0 442.7-.9 448.3 .9 453.4s6 8.9 11.2 10.2l64 16c4.2 1.1 8.7 .3 12.4-2s6.3-6.1 7.1-10.4c8.6-42.8 4.3-81.2-2.1-108.7C90.3 344.3 86 329.8 80 316.5V291.9c0-30.2 10.2-58.7 27.9-81.5c12.9-15.5 29.6-28 49.2-35.7l157-61.7c8.2-3.2 17.5 .8 20.7 9s-.8 17.5-9 20.7l-157 61.7c-12.4 4.9-23.3 12.4-32.2 21.6l159.6 57.6c7.6 2.7 15.6 4.1 23.7 4.1s16.1-1.4 23.7-4.1L624.2 182.6c9.5-3.4 15.8-12.5 15.8-22.6s-6.3-19.1-15.8-22.6L343.7 36.1C336.1 33.4 328.1 32 320 32zM128 408c0 35.3 86 72 192 72s192-36.7 192-72L496.7 262.6 354.5 314c-11.1 4-22.8 6-34.5 6s-23.5-2-34.5-6L143.3 262.6 128 408z"/></svg>
    </i>    
    
    College Credit',
    
    '<i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 1.5deg;">
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="15" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M346.3 271.8l-60.1-21.9L214 448H32c-17.7 0-32 14.3-32 32s14.3 32 32 32H544c17.7 0 32-14.3 32-32s-14.3-32-32-32H282.1l64.1-176.2zm121.1-.2l-3.3 9.1 67.7 24.6c18.1 6.6 38-4.2 39.6-23.4c6.5-78.5-23.9-155.5-80.8-208.5c2 8 3.2 16.3 3.4 24.8l.2 6c1.8 57-7.3 113.8-26.8 167.4zM462 99.1c-1.1-34.4-22.5-64.8-54.4-77.4c-.9-.4-1.9-.7-2.8-1.1c-33-11.7-69.8-2.4-93.1 23.8l-4 4.5C272.4 88.3 245 134.2 226.8 184l-3.3 9.1L434 269.7l3.3-9.1c18.1-49.8 26.6-102.5 24.9-155.5l-.2-6zM107.2 112.9c-11.1 15.7-2.8 36.8 15.3 43.4l71 25.8 3.3-9.1c19.5-53.6 49.1-103 87.1-145.5l4-4.5c6.2-6.9 13.1-13 20.5-18.2c-79.6 2.5-154.7 42.2-201.2 108z"/></svg>
    </i> 
    
    Summer Program',
    
    '<i class="icon" aria-hidden="true" style="position: relative; top: -1px;">
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M495.8 0c5.5 0 10.9 .2 16.3 .7c7 .6 12.8 5.7 14.3 12.5s-1.6 13.9-7.7 17.3c-44.4 25.2-74.4 73-74.4 127.8c0 81 65.5 146.6 146.2 146.6c8.6 0 17-.7 25.1-2.1c6.9-1.2 13.8 2.2 17 8.5s1.9 13.8-3.1 18.7c-34.5 33.6-81.7 54.4-133.6 54.4c-9.3 0-18.4-.7-27.4-1.9c-11.2-22.6-29.8-40.9-52.6-51.7c-2.7-58.5-50.3-105.3-109.2-106.7c-1.7-10.4-2.6-21-2.6-31.8C304 86.1 389.8 0 495.8 0zM447.9 431.9c0 44.2-35.8 80-80 80H96c-53 0-96-43-96-96c0-47.6 34.6-87 80-94.6l0-1.3c0-53 43-96 96-96c34.9 0 65.4 18.6 82.2 46.4c13-9.1 28.8-14.4 45.8-14.4c44.2 0 80 35.8 80 80c0 5.9-.6 11.7-1.9 17.2c37.4 6.7 65.8 39.4 65.8 78.7z"/></svg>
    </i>

    Overnight',
    
    '
    <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right:-4px;">
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M160 0c17.7 0 32 14.3 32 32V67.7c1.6 .2 3.1 .4 4.7 .7c.4 .1 .7 .1 1.1 .2l48 8.8c17.4 3.2 28.9 19.9 25.7 37.2s-19.9 28.9-37.2 25.7l-47.5-8.7c-31.3-4.6-58.9-1.5-78.3 6.2s-27.2 18.3-29 28.1c-2 10.7-.5 16.7 1.2 20.4c1.8 3.9 5.5 8.3 12.8 13.2c16.3 10.7 41.3 17.7 73.7 26.3l2.9 .8c28.6 7.6 63.6 16.8 89.6 33.8c14.2 9.3 27.6 21.9 35.9 39.5c8.5 17.9 10.3 37.9 6.4 59.2c-6.9 38-33.1 63.4-65.6 76.7c-13.7 5.6-28.6 9.2-44.4 11V480c0 17.7-14.3 32-32 32s-32-14.3-32-32V445.1c-.4-.1-.9-.1-1.3-.2l-.2 0 0 0c-24.4-3.8-64.5-14.3-91.5-26.3c-16.1-7.2-23.4-26.1-16.2-42.2s26.1-23.4 42.2-16.2c20.9 9.3 55.3 18.5 75.2 21.6c31.9 4.7 58.2 2 76-5.3c16.9-6.9 24.6-16.9 26.8-28.9c1.9-10.6 .4-16.7-1.3-20.4c-1.9-4-5.6-8.4-13-13.3c-16.4-10.7-41.5-17.7-74-26.3l-2.8-.7 0 0C119.4 279.3 84.4 270 58.4 253c-14.2-9.3-27.5-22-35.8-39.6c-8.4-17.9-10.1-37.9-6.1-59.2C23.7 116 52.3 91.2 84.8 78.3c13.3-5.3 27.9-8.9 43.2-11V32c0-17.7 14.3-32 32-32z"/></svg>
    </i>

    Financial Assistance',
);

$features_var = array(
    "College-Credit",
    "Summer-Program",
    "Overnight",
    "Financial-Assistance",
);

$filter_btn = '
<div class="filter_btn col-12 col-md-auto">
<button class="btn btn-theme btn-theme-outline btn-theme-outline-primary btn-theme-small m-1" type="button" data-bs-toggle="collapse" data-bs-target="#filter-options" aria-expanded="false" aria-controls="filter-options">

    <i class="filter__icon" aria-hidden="true" style="position: relative; left: -2px;">
        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-filter" viewBox="0 0 16 16"> <path d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5m-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5m-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5"/></svg>
    </i>

    Filter
</button>
</div>
';

$filter_btn_reversed = '
<div class="filter_btn col-12 col-md-auto">
<button class="btn btn-theme btn-theme-outline btn-theme-outline-reversed btn-theme-small m-1" type="button" data-bs-toggle="collapse" data-bs-target="#filter-options" aria-expanded="false" aria-controls="filter-options">

    <i class="filter__icon" aria-hidden="true" style="position: relative; left: -2px;">
        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-filter" viewBox="0 0 16 16"> <path d="M6 10.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5m-2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5m-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5"/></svg>
    </i>

    Filter
</button>
</div>
';

$share_btn = '
<div class="share_btn col-12 col-md-auto">
<button class="share-button btn btn-theme btn-theme-outline btn-theme-outline-primary btn-theme-small m-1" type="button">
    
    Share

    <i class="share__icon" aria-hidden="true" style="position: relative; left: 2px; top: -3px;">
        <svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 -960 960 960" width="24px" fill="currentColor"><path d="M240-40q-33 0-56.5-23.5T160-120v-440q0-33 23.5-56.5T240-640h80q17 0 28.5 11.5T360-600q0 17-11.5 28.5T320-560h-80v440h480v-440h-80q-17 0-28.5-11.5T600-600q0-17 11.5-28.5T640-640h80q33 0 56.5 23.5T800-560v440q0 33-23.5 56.5T720-40H240Zm200-727-36 36q-12 12-28 11.5T348-732q-11-12-11.5-28t11.5-28l104-104q12-12 28-12t28 12l104 104q11 11 11 27.5T612-732q-12 12-28.5 12T555-732l-35-35v407q0 17-11.5 28.5T480-320q-17 0-28.5-11.5T440-360v-407Z"/></svg>
    </i>

</button>
</div>
';

$location = array(
    '
    MSU Musesum <br />
    409 W Circle Dr <br />
    East Lansing <br />
    MI 48824 <br />
    ',

    '
    Munn Ice Arena <br />
    509 Birch Rd <br />
    East Lansing <br />
    MI 48824 <br />
    ',

    '
    Abrams Planetarium<br />
    755 Science Rd <br />
    East Lansing <br />
    MI 48824 <br />
    ',

    '
    Breslin Center<br />
    534 Birch Rd <br />
    East Lansing <br />
    MI 48824 <br />
    ',

    '
    STEM Teaching and Learning Facility<br />
    642 Red Cedar Rd <br />
    East Lansing <br />
    MI 48824 <br />
    ',

    
);



error_reporting(E_ERROR | E_PARSE);
$datafile = fopen("Content/SYPresources.csv", "r");

            if ($datafile !== FALSE) {
                while (! feof($datafile)) {
                    $data = fgetcsv($datafile, 1000, ",");
                
                    include("Views/Shared/Partials/data-map.php");

                    if (! empty($data) && $dataID == $_GET["ID"]) {
                    
                    $resource_title = $title;
                    $resourceID = $dataID;

                    }
                }
            }
            fclose($datafile);

?>
