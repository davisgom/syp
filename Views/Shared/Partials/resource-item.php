<div class="row">
<div class="col-12 resource d-md-flex">
        <div class="col-auto me-3 mb-3 d-none d-md-block resource-img">
            <img src="<?php if (! empty($resourceImg)){echo 'upload/'.$resourceImg;}else{echo'Content/Images/syp-placeholder-'.$syp__color[array_rand($syp__color)].'.svg';}?>" class="img-fluid rounded ratio ratio-4x3" />
        </div>   
        
        <div class="col-lg">
        <h2><a href="resource?ID=<?php echo $dataID; ?>"><?php echo $title; ?></a></h2>
        
        <div class="dates <?php if(!empty($dates)){echo "d-block";} else { echo "d-none";} ?>"> 
            <i class="date-info__icon me-1" aria-hidden="true" style="position: relative; top: -2px; left:-3px;">
                <svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 0 18 20" width="24px" fill="currentColor"><path d="M8,13.4l3.5-3.5c.2-.2.4-.3.7-.3s.5.1.7.3c.2.2.3.4.3.7s-.1.5-.3.7l-4.2,4.2c-.2.2-.4.3-.7.3s-.5-.1-.7-.3l-2.1-2.1c-.2-.2-.3-.4-.3-.7s.1-.5.3-.7c.2-.2.4-.3.7-.3s.5.1.7.3c0,0,1.4,1.4,1.4,1.4ZM2,20c-.6,0-1-.2-1.4-.6s-.6-.9-.6-1.4V4c0-.6.2-1,.6-1.4.4-.4.9-.6,1.4-.6h1v-1c0-.3,0-.5.3-.7.2-.2.4-.3.7-.3s.5,0,.7.3.3.4.3.7v1h8v-1c0-.3,0-.5.3-.7s.4-.3.7-.3.5,0,.7.3.3.4.3.7v1h1c.6,0,1,.2,1.4.6s.6.9.6,1.4v14c0,.6-.2,1-.6,1.4s-.9.6-1.4.6H2ZM2,18h14v-10H2v10ZZ"/></svg>
            </i> 

            <?php echo $dates; ?>
        </div>
    

        <?php include("Views/Shared/Partials//tags-list.php") ?>

        <div class="blurb-short">
            <?php echo $blurb; ?> 
        </div>
        </div>
    </div>
</div>