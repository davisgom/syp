<footer class="uoe-footer uoe-footer-dark">
	<div class="container">
		<div class="row">
			<div class="col-12 col-xl-8 mb-4 d-md-flex">
				<h2 class="uoe-footer-logo">
					<span class="visually-hidden">
						University Outreach and Engagement
					</span>
				</h2>

				<p>
					The Office of University Outreach and Engagement facilitates university-wide efforts by supporting the engaged activities of faculty, staff, and students; fostering public access to university expertise and resources; and by advocating for exemplary engaged scholarship.
				</p>
			</div>

			
			<div class="social-footer col-12 col-xl-4">
			
				<h3>UOE Social Links</h3>

				<ul class="spaced-list">
					<li>			
						<a href="https://www.facebook.com/msuengage" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/facebook.svg">
							UOE Facebook
						</a>
					</li>

					<li>
						<a href="https://www.instagram.com/msuengage/" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/instagram.svg">
							UOE Instagram
						</a>
					</li>
					
					<li>
						<a href="#" class="" role="menuitem">
							<img src="/Content/Images/social/linkedin.svg">
							UOE LinkedIn
						</a>
					</li>

					
					<li>
						<a href="https://www.youtube.com/user/uoecit" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/youtube.svg">
							UOE Youtube
						</a>
					</li>
					
					<li>
						<a href="https://twitter.com/MSU_UOE" class="" role="menuitem" target="_blank">
							<img src="/Content/Images/social/x-twitter.svg">
							UOE Twitter
						</a>
					</li>

					<li>
						<a href="#" class="" role="menuitem">
							<img src="/Content/Images/social/envelope.svg">
							UOE Newsletter
						</a>
					</li>
				</ul>
			</div>
    	</div>
	</div>
</footer>