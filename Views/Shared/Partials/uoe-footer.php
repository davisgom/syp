<footer class="uoe-footer uoe-footer-dark">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 col-xxl-5 mb-4">
        <h2 class="uoe-footer-logo"><span class="visually-hidden">University Outreach and Engagement</span></h2>

        <p>
        The Office of University Outreach and Engagement facilitates university-wide efforts by supporting the engaged activities of faculty, staff, and students; fostering public access to university expertise and resources; and by advocating for exemplary engaged scholarship.
        </p>
      </div>

      <div class="col-12 col-lg-auto">
      </div>
      
      <div class="col-12 col-lg-2 align-items-center">

	  <h3>UOE Quick Links</h3>

          <ul class="align-self-end">
  						<li role="none" class="">
  							<a href="#" role="menuitem">Overview</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Departments</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE Projects</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Looking for Partners?</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Public Events and Programs</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">UOE People</a>
  						</li>
  						<li role="none" class="">
  							<a href="#" role="menuitem">Contact</a>
  						</li>
  				</ul>
        </div>

		<div class="col-12 col-lg-2 social-footer">
		
		<h3>UOE Social Links</h3>

			<ul class="spaced-list">
				<li>
					<a href="/about/newsletter-subscription" class="" role="menuitem">
						<img src="/Content/Images/social/envelope.svg">
						UOE Events Newsletter
					</a>
				</li>
				<li>			
					<a href="https://www.facebook.com/msuengage" class="" role="menuitem" target="_blank">
						<img src="/Content/Images/social/facebook.svg">
						UOE Facebook
					</a>
				</li>
				<li>
					<a href="https://twitter.com/MSU_UOE" class="" role="menuitem" target="_blank">
						<img src="/Content/Images/social/x-twitter.svg">
						UOE Twitter
					</a>
				</li>
				<li>
					<a href="https://www.instagram.com/msuengage/" class="" role="menuitem" target="_blank">
						<img src="/Content/Images/social/instagram.svg">
						UOE Instagram
					</a>
				</li>
				<li>
					<a href="https://www.youtube.com/user/uoecit" class="" role="menuitem" target="_blank">
						<img src="/Content/Images/social/youtube.svg">
						UOE Youtube
					</a>
				</li>
			</ul>
		</div>

    </div>
  </div>
</footer>