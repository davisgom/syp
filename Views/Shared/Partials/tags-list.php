<ul class="tags">
    <!-- Grade Tags -->
    <li class="tag <?php if ($gpre_k == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-grade">Pre-K</span>
    </li>

    <li class="tag <?php if ($gk_2 == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-grade">K-2</span>
    </li>

    <li class="tag <?php if ($g3_5 == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-grade">3-5</span>
    </li>

    <li class="tag <?php if ($g6_8 == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-grade">6-8</span>
    </li>

    <li class="tag <?php if ($g9_12 == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-grade">9-12</span>
    </li>

    <!-- Topic Tags -->
    <li class="tag <?php if ($topic_ag == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Agriculture</span>
    </li>

    <li class="tag <?php if ($topic_an == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Animal Care</span>
    </li>

    <li class="tag <?php if ($topic_art == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Art, Drama, & Music</span>
    </li>

    <li class="tag <?php if ($topic_bus == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Business</span>
    </li>

    <li class="tag <?php if ($topic_cp == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">College Preparation</span>
    </li>

    <li class="tag <?php if ($topic_com == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Computers & Technology</span>
    </li>

    <li class="tag <?php if ($topic_eng == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Engineering</span>
    </li>

    <li class="tag <?php if ($topic_env == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Environtmental Sciences & Natural Resources</span>
    </li>

    <li class="tag <?php if ($topic_led == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Leadership Skills</span>
    </li>

    <li class="tag <?php if ($topic_mat == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Math & Science</span>
    </li>

    <li class="tag <?php if ($topic_med == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Medical Sciences</span>
    </li>

    <li class="tag <?php if ($topic_soc == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Social Studies</span>
    </li>

    <li class="tag <?php if ($topic_spr == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Sports, Fitness, & Nutrition</span>
    </li>

    <li class="tag <?php if ($topic_wrt == "1") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-topic">Writing & Language</span>
    </li>
    
    <!-- Feature Tags -->
    <li class="tag <?php if ($college == "TRUE") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-feature"><?php echo $features[0]; ?></span>
    </li>

    <li class="tag <?php if ($summer == "TRUE") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-feature"><?php echo $features[1]; ?></span>
    </li>

    <li class="tag <?php if ($overnight == "TRUE") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-feature"><?php echo $features[2]; ?></span>
    </li>

    <li class="tag <?php if ($scholarship == "TRUE") {echo "d-inline-block";} else {echo "d-none";} ?>">
        <span class="tag-item btn-tag tag-outline-feature"><?php echo $features[3]; ?></span>
    </li>
</ul>