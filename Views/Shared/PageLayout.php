<?php ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean(); ?>

<?php if (isset($page_title)){ $set_page_title = $page_title;}?>

<header class="page-header <?php echo $page_content . '-header'; ?>">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-sm-11 d-flex">
        <h1>
          <span class="page-title <?php if (isset($header_class)){ echo $header_class; } ?>">
            <?php if (isset($page_title)){ echo $page_title; } else echo $page_content; ?>
          </span>
        </h1>
      </div>
    </div>

    <div class="row align-items-center">
      <div class="col-12 d-md-flex header-content">
        <?php if (isset($header_content)){ echo $header_content; }?>
      </div>
    </div>
  </div>
</header>

<?php if(isset($include_filters)){include ("Views/Shared/Partials/filter-options.php");} ?>

<section class="container">
  <div class="row justify-content-end">
    <?php if ($page_content == "resource") {echo $share_btn;} ?>
  </div>

  <div class="row">
    <div class="col-12 mt-2 mt-md-5">
      <?php echo $content ?>
    </div>
  </div>
</section>
