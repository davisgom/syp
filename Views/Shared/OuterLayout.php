<?php include("Views/Shared/Partials/global-vars.php"); ?>

<!doctype html>
<html lang="en-US">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <meta name="author" content="<?php echo $author; ?>" />
    <meta name="description" content="<?php echo $description; ?>" />

    <link rel="stylesheet" href="Content/Site.css?v=<?php echo date("mdYHisue");?>" />

    <link rel="SHORTCUT ICON" href="Content/Images/favicon.ico" />

    <title>
      <?php
        if ($page_content == "resource") {echo $resource_title;} else echo str_replace("-", " ", ucfirst($page_content)); ?>
       -
      <?php echo $site_title; ?>
    </title>
</head>

<body class="<?php echo $page_content; if ($page_content != "home") {echo " page";}?>">

<!-- <div class="center-line"></div> -->

    <!-- Include the header. This includes the MSU masthead and the site header. -->
    <?php include("Views/Shared/Partials/header.php"); ?>

    <main id="Main" class="site-content">
        <!-- Include the page content. This looks for the pages in "Content/Pages" and displays the page content. -->

        <?php
          if ($page_content == "home") {
            include ("Content/Pages/$page_content.php");
          }

          if ($page_content != "home") {
            include ("PageLayout.php");
          }
        ?>
    </main>

    <!-- Include the footer. This includes the MSU footer and the site footer. -->
    <?php include("Views/Shared/Partials/footer.php"); ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="Scripts/jquery-3.3.1.slim.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    
    <script type="text/javascript" src="Scripts/bootstrap.bundle.min.js"></script>



    <script type="text/javascript">
    	$(function () { $("[data-toggle='tooltip']").tooltip(); });
    	$(document).ready(function(){$('[data-toggle="popover"]').popover(); });

      const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
      const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

    	function navToggle() {
        var x = document.getElementById("siteNav");

        if (x.className === "overlay") {
            x.className += " overlay-open";
          } else {
            x.className = "overlay";
        }
      }

      function normalViewToggle() {
        var x = document.getElementById("resourceView");

        if (x.className === "resource-view-small") {
            x.className -= " resource-view-small";
            x.className += " resource-view-normal";
        }
        else if (x.className === "resource-view-tiny") {
            x.className -= " resource-view-tiny";  
            x.className += " resource-view-normal";
        }  
        else {
            x.className = " resource-view-normal";
        }
      }
      
      function smallViewToggle() {
        var x = document.getElementById("resourceView");

        if (x.className === "resource-view-normal") {
            x.className -= " resource-view-normal";
            x.className += " resource-view-small";
        }
        else if (x.className === "resource-view-tiny") {
            x.className -= " resource-view-tiny";  
            x.className += " resource-view-small";
        }  
        else {
            x.className = " resource-view-small";
        }
      }

      function tinyViewToggle() {
        var x = document.getElementById("resourceView");

        if (x.className === "resource-view-normal") {
            x.className -= " resource-view-normal";
            x.className += " resource-view-tiny";
        }
        else if (x.className === "resource-view-small") {
            x.className -= " resource-view-small";  
            x.className += " resource-view-tiny";
        }  
        else {
            x.className = " resource-view-tiny";
        }
      }

      const shareButton = document.querySelector('.share-button');
      const shareDialog = document.querySelector('.share-dialog');
      const closeButton = document.querySelector('.close-button');

      shareButton.addEventListener('click', event => {
        if (navigator.share) { 
        navigator.share({
            title: '<?php if ($page_content == "resource") {echo $resource_title;} else echo str_replace("-", " ", ucfirst($page_content)); ?> - <?php echo $site_title; ?>',
            url: 'https://syp.mdavis.in/resource?ID=<?php echo $resourceID; ?>'
          }).then(() => {
            console.log('Thanks for sharing!');
          })
          .catch(console.error);
          } 
      });
    </script>
</body>
</html>
