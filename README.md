# Spartan Youth Programs

This is the prototype/desgin for the new Spartan Youth Programs website. 
You can view the live design at: http://syp.mdavis.in

Currently it uses
- Bootstrap v5.3.2 (https://getbootstrap.com/)
- SCSS
- PHP
- Codekit (To compile SCSS on local machine)
