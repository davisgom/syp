<?php

$page_title="Scholarships";

?>

<div class="page-content">
  <p>
    Students who will enter 8th, 9th, or 10th grades following their involvement in an MSU pre-college program are eligible for nomination to apply for a $2,000 Pre-College Achievement Scholarships (PAS). A maximum of 60 scholarships are awarded each year and students are only eligible to apply once to win. The scholarship will be applied toward the student's first year at MSU as a degree-seeking student. Scholarship recipients are not guaranteed admission to MSU and are expected to meet admission requirements following their junior year of high school.
  </p>

  <p>
    Please check with your pre-college program director to learn more about eligibility and nomination within your program and for a copy of the application. Pre-college program directors may nominate up to 5% of the program participants in these grades for scholarship consideration. Completed applications are due by November 1.
  </p>

  <p>
    Students must also provide a copy of their full year report card or transcript from their most recently completed grade.
  </p>

  <p>
    For other scholarship information visit the MSU Admissions office Web site
  </p>

</div>

<div class="col-11 col-md-auto program-match mt-1 mb-5">
    <?php include("Views/Shared/Partials/program-match.php"); ?>
</div>