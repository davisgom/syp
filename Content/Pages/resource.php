<?php 
error_reporting(E_ERROR | E_PARSE);

$datafile = fopen("Content/SYPresources.csv", "r");

            if ($datafile !== FALSE) {
                while (! feof($datafile)) {
                    $data = fgetcsv($datafile, 1000, ",");
                
                    include("Views/Shared/Partials/data-map.php");

                    if (! empty($data) && $dataID == $_GET["ID"]) {

                    $page_title = $title;

?>

<section class="container mb-5">
  <div class="row pt-0 mb-0">
    <div class="col-12 col-md-7 col-xl-8 pe-md-4 order-2 order-md-1">
        <div class="alerts">
            <div class="alert alert-warning d-flex alert-dismissible fade show">
              <i class="icon me-1" aria-hidden="true" style="position: relative; top: -2px; left: -3px;">
              <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="currentColor"><path d="M200-80q-33 0-56.5-23.5T120-160v-560q0-33 23.5-56.5T200-800h40v-80h80v80h320v-80h80v80h40q33 0 56.5 23.5T840-720v560q0 33-23.5 56.5T760-80H200Zm0-80h560v-400H200v400Zm0-480h560v-80H200v80Zm0 0v-80 80Zm80 240v-80h400v80H280Zm0 160v-80h280v80H280Z"/></svg>
              </i>
              
              <span>
                <strong>This program typically re-occurs, but some or all the dates may have passed.</strong>
                  See below for details or check back later for new program dates.
              </span>

              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <div class="alert alert-danger d-flex alert-dismissible fade show">
              <i class="icon me-1" aria-hidden="true" style="position: relative; top: -2px; left: -3px;">
                <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" ><path fill="currentColor" d="M200-640h560v-80H200v80Zm0 0v-80 80Zm0 560q-33 0-56.5-23.5T120-160v-560q0-33 23.5-56.5T200-800h40v-80h80v80h320v-80h80v80h40q33 0 56.5 23.5T840-720v227q-19-9-39-15t-41-9v-43H200v400h252q7 22 16.5 42T491-80H200Zm520 40q-83 0-141.5-58.5T520-240q0-83 58.5-141.5T720-440q83 0 141.5 58.5T920-240q0 83-58.5 141.5T720-40Zm67-105 28-28-75-75v-112h-40v128l87 87Z"/></svg>
              </i>
              
              <span>
                <strong>Registration Deadline</strong> 
                
                <ul class="mb-0">
                  <li>Session 1 application deadline is June 14</li>
                  <li>Session 2 application deadline is June 21</li>
                  <li>Session 3 application deadline is July 5</li>
                </ul>
              </span>

              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <div class="alert alert-info d-flex alert-dismissible fade show">
              <i class="icon me-1" aria-hidden="true" style="position: relative; top: -2px; left: -3px;">
                <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 -960 960 960" width="24px" fill="#currentColor"><path d="M480-280q17 0 28.5-11.5T520-320v-160q0-17-11.5-28.5T480-520q-17 0-28.5 11.5T440-480v160q0 17 11.5 28.5T480-280Zm0-320q17 0 28.5-11.5T520-640q0-17-11.5-28.5T480-680q-17 0-28.5 11.5T440-640q0 17 11.5 28.5T480-600Zm0 520q-83 0-156-31.5T197-197q-54-54-85.5-127T80-480q0-83 31.5-156T197-763q54-54 127-85.5T480-880q83 0 156 31.5T763-763q54 54 85.5 127T880-480q0 83-31.5 156T763-197q-54 54-127 85.5T480-80Zm0-80q134 0 227-93t93-227q0-134-93-227t-227-93q-134 0-227 93t-93 227q0 134 93 227t227 93Zm0-320Z"/></svg>
              </i>
              
              <span>
                This is what the Important Note for a resource should look like. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et.
              </span>

              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            
        </div>
        
        <div class="resource-data mb-3">
              <?php include("Views/Shared/Partials/tags-list.php") ?>
        </div>

        <div class="blurb">
            <?php echo $blurb; ?> 
        </div>
    </div>

    <div class="col-12 col-md-5 col-xl-4 mb-auto order-1 order-md-2">
        
        <div class="resource-info">
          <h3 class="h5">Full Program Details <br /> and Registration</h3>

          <hr />

          <a class="btn btn-theme btn-theme-syp-link w-100 justify-content-center" href="#">
            <small>Visit Program Page
              <i class="icon" aria-hidden="true" style="position: relative; top: -1.5px;">
                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 448 512"> <!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M384 32c35.3 0 64 28.7 64 64V416c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V96C0 60.7 28.7 32 64 32H384zM160 144c-13.3 0-24 10.7-24 24s10.7 24 24 24h94.1L119 327c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l135-135V328c0 13.3 10.7 24 24 24s24-10.7 24-24V168c0-13.3-10.7-24-24-24H160z"/></svg>
              </i>
            </small>
          </a>

          <hr />

          <div class="date mb-2 d-flex">
            <i class="date-info__icon" aria-hidden="true" style="position: relative; top: -2px; left: -3px;">
              <svg xmlns="http://www.w3.org/2000/svg" height="20px" viewBox="0 0 18 20" width="24px"><path fill="currentColor" d="M8,13.4l3.5-3.5c.2-.2.4-.3.7-.3s.5.1.7.3c.2.2.3.4.3.7s-.1.5-.3.7l-4.2,4.2c-.2.2-.4.3-.7.3s-.5-.1-.7-.3l-2.1-2.1c-.2-.2-.3-.4-.3-.7s.1-.5.3-.7c.2-.2.4-.3.7-.3s.5.1.7.3c0,0,1.4,1.4,1.4,1.4ZM2,20c-.6,0-1-.2-1.4-.6s-.6-.9-.6-1.4V4c0-.6.2-1,.6-1.4.4-.4.9-.6,1.4-.6h1v-1c0-.3,0-.5.3-.7.2-.2.4-.3.7-.3s.5,0,.7.3.3.4.3.7v1h8v-1c0-.3,0-.5.3-.7s.4-.3.7-.3.5,0,.7.3.3.4.3.7v1h1c.6,0,1,.2,1.4.6s.6.9.6,1.4v14c0,.6-.2,1-.6,1.4s-.9.6-1.4.6H2ZM2,18h14v-10H2v10ZZ"/></svg>
            </i>

            <?php if(!empty($dates)){echo str_replace(" | ", "<br />",$dates);} ?>
          </div>

          <hr />

          <div class="program-fee mb-2 d-flex">
            <i class="icon" aria-hidden="true" style="position: relative; left: -3px; top: -1px;">
              <svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960" width="24px" fill="currentColor"><path d="M560-440q-50 0-85-35t-35-85q0-50 35-85t85-35q50 0 85 35t35 85q0 50-35 85t-85 35ZM280-320q-33 0-56.5-23.5T200-400v-320q0-33 23.5-56.5T280-800h560q33 0 56.5 23.5T920-720v320q0 33-23.5 56.5T840-320H280Zm80-80h400q0-33 23.5-56.5T840-480v-160q-33 0-56.5-23.5T760-720H360q0 33-23.5 56.5T280-640v160q33 0 56.5 23.5T360-400Zm400 240H120q-33 0-56.5-23.5T40-240v-400q0-17 11.5-28.5T80-680q17 0 28.5 11.5T120-640v400h640q17 0 28.5 11.5T800-200q0 17-11.5 28.5T760-160ZM280-400v-320 320Z"/></svg>
            </i>

            <strong>Program Fee:</strong> &nbsp; $123

          </div>

          <hr />

          <div class="location mb-2 d-flex">
            <i class="icon" aria-hidden="true" style="position: relative; left: 1px; top: -1px;">
              <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 448 512"> <!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M215.7 499.2C267 435 384 279.4 384 192C384 86 298 0 192 0S0 86 0 192c0 87.4 117 243 168.3 307.2c12.3 15.3 35.1 15.3 47.4 0zM192 128a64 64 0 1 1 0 128 64 64 0 1 1 0-128z"/></svg>
            </i>

            <address class="ms-1" style="line-height: 1.5;">
              <?php echo $location[array_rand($location)];?>
            </address>
          </div>

          <hr />

          <h3 class="h5"> Contact Information </h3>

          <ul class="list-unstyled ps-2">
            <li><strong> <?php echo $cname[array_rand($cname)];?> Spartan </strong></li>
            <li><strong>Phone:</strong> 1-517-355-1855</li>
            <li><strong>E-mail:</strong> <a href="#">spartan@msu.edu</a></li>
          </ul>
        </div>

        <div class="my-4">         
          <img src="<?php if (! empty($resourceImg)){echo 'upload/'.$resourceImg;}else{echo'Content/Images/syp-placeholder-'.$syp__color[array_rand($syp__color)].'.svg';}?>" class="img-fluid rounded" />
        </div>

    </div>
  </div>
</section>

<?php
      }
      }
      }
  fclose($datafile);
?>