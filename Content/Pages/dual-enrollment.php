<?php

$page_title = "College Courses for High School Students";

?>


<div class="page-content">


<p>Dual Enrollment (DE) is an opportunity for students in grades 9.5-12 to enroll in select undergraduate coursesat Michigan State University while in high school. Students interested in Dual Enrollment may choose this option because they:</p>

<ul>
	<li>Have exhausted the advance course options in their high school.</li>
	<li>Are interested in exploring an elective not available to them at their home school.</li>
	<li>Are nearing the end of their high school career and want to take an introductory course in a subject to see if that course is worth pursuing in their collegiate career.</li>
</ul>

<p>Whatever the motivation, Dual Enrollment at MSU is a great way for a student to slowly acclimate themselves to collegiate studies.</p>

<p>For full details visit the <a href="#">MSU Dual Enrollment Website</a>.</p>


<p>
  <a href="#" class="btn btn-theme btn-theme-syp-link">
	MSU Dual Enrollment Website &nbsp;

	<i class="date-info__icon" aria-hidden="true" style="position: relative; top: -2px;">
		<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 448 512"> <!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M384 32c35.3 0 64 28.7 64 64V416c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V96C0 60.7 28.7 32 64 32H384zM160 144c-13.3 0-24 10.7-24 24s10.7 24 24 24h94.1L119 327c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0l135-135V328c0 13.3 10.7 24 24 24s24-10.7 24-24V168c0-13.3-10.7-24-24-24H160z"/></svg>
	</i>
</a>
</p>

<br />

</div>