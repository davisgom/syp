<?php

$page_title="About SYP";

?>

<div class="page-content">
  <p>
    Spartan Youth Programs displays a wide range of exciting opportunities for youth to improve their knowledge in specific subject areas. Programs are available to serve all age ranges from pre-kindergarten to high school.
  </p>

  <p>
    Browse the site to find descriptions of programs that interest you and your child. Each description includes complete contact information for your convenience. To register for a specific program, simply go to that program's Web site.
  </p>

  <p>
    With well over 200 different programs covering topics in agriculture, art, business, computers, engineering, math, music, science, sports, and writing, MSU is sure to have a program for every student. Pre-college programs are an excellent way for students to explore potential majors or careers while being introduced to the college environment.
  </p>

  <p>
    <ul>
      <li>
        Programs are offered during the summer and during the school year.
      </li>

      <li>
        Several programs offer high school students the opportunity to earn college credit.
      </li>

      <li>
        Many programs offer students the opportunity to stay on the MSU campus during the program.
      </li>

      <li>
        Financial Aid is available for some programs.
      </li>
    </ul>
  </p>

  <hr />

  <p>
    The MSU WorkLife Office maintains a guide on things that your family can do at MSU. View the <a href="https://worklife.msu.edu/publication/things-to-do-at-msu" class="external" target="_blank">Things to Do at MSU Activity Guide for Families</a>.
  </p>

  <p>
    Visit the <a href="https://worklife.msu.edu/" class="external" target="_blank">WorkLife Office</a> website where you can find information about services and community resources.
  </p>

  <hr />

  <h2>
    Help &amp; Support
  </h2>

  <p>
    The Spartan Youth Programs Web site is maintained by <a href="http://outreach.msu.edu" target="_blank" title="University Outreach and Engagement, Web site - opens in new window" class="nospace external">University Outreach and Engagement (UOE)</a>.  <abbr title="University Outreach and Engagement">UOE</abbr> achieves MSU's land-grant mission by bringing the benefits of University knowledge to the public.
  </p>

  <h3 class="h4">For more information:</h3>
  
  <p>
    E-mail Spartan Youth Programs at <a href="mailto:youth@msu.edu" title="E-mail youth@msu.edu" target="_blank" class="nospace email"><strong>youth@msu.edu</strong></a>
  </p>

  <h3 class="h4">For assistance any time:</h3>
  
  <p>
    Call the toll-free 24-hour <abbr title="Michigan State University">MSU</abbr> Libraries, Computing, and Technology Helpline at <strong>1-800-500-1554</strong>.
  </p>

  <br />

</div>

<div class="col-11 col-md-auto program-match mt-1 mb-5">
    <?php include("Views/Shared/Partials/program-match.php"); ?>
</div>