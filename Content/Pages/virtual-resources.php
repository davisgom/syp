<?php

$page_title='Virtual Resources <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 2deg;">
<svg xmlns="http://www.w3.org/2000/svg" width="46" height="36" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M16 64C16 28.7 44.7 0 80 0H304c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H80c-35.3 0-64-28.7-64-64V64zM144 448c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16s-7.2-16-16-16H160c-8.8 0-16 7.2-16 16zM304 64H80V384H304V64z"/></svg>
</i>';

$header_content = '
<div class="col-12 col-md-9 me-md-auto">
    <p>
        Browse virtual resources available for <span class="text-nowrap">pre-kindergarten</span> to high school learners.
    </p>
</div>

'.$filter_btn_reversed.'

';

$include_filters = "true";
?>

<?php include("Views/Shared/Partials/view-controls.php"); ?>

<div id="resourceView" class="resource-view-normal">
    <section class="resource-data container">
    <?php   
                error_reporting(E_ERROR | E_PARSE);
                
                $datafile = fopen("Content/SYPresources.csv", "r");;
                
                if ($datafile !== FALSE) {
                    while (! feof($datafile)) {
                        $data = fgetcsv($datafile, 1000, ",");
                    
                        include("Views/Shared/Partials/data-map.php");
                        
                        if (! empty($data) && $status == "Active" && $resource  == "1") {
            ?>
            
                <?php include("Views/Shared/Partials/resource-item.php") ?>

            <?php
            }
            }
            }
            fclose($datafile);
            ?>
    </section>
</div>