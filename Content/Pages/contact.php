<div class="page-content">
  <p>
    The Office of <a href="http://outreach.msu.edu" target="_blank" title="University Outreach and Engagement, Web site - opens in new window" class="nospace external">University Outreach and Engagement (UOE)</a> maintains the Spartan Youth Programs website, which catalogs youth-oriented programs offered by many units at MSU. <strong>Resources listed in this site are updated as information is made available from each unit</strong>. To receive more information about any of the resources listed, please refer to the contact information given for that resource.
  </p>

  <h2 class="h4">For more information about this website:</h3>
  
  <ul class="list-unstyled">
    <li><strong>Emily Springer</strong></li>
    <li>Communications Manager I</li>
    <li>Communication and Information Technology</li>
    <li>University Outreach and Engagement</li>
    <li>219 S. Harrison Road, Room 93</li>
    <li>East Lansing, MI 48824</li>
    <li class="mt-3">Phone: 517-353-8977</li>
    <li>Fax: 517-432-9541</li>
    <li><a href="mailto:youth@msu.edu" class="email" target="_blank"><strong>youth@msu.edu</strong></a></li>
  </ul>

  <br />
  <br />

  <h2 class="h4">For assistance any time:</h3>
  
  <p>
    Call the toll-free 24-hour <abbr title="Michigan State University">MSU</abbr> Libraries, Computing, and Technology Helpline at <strong>1-800-500-1554</strong>.
  </p>

  <br />
  <br />

</div>