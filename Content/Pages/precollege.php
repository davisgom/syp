<?php

$page_title="Pre-College Committee";

?>

<div class="page-content">
  <p>
    The Pre-College Committee at MSU is comprised of program directors, coordinators, and senior administrators who work to coordinate the activities of pre-college programs at MSU. The committee provides guidance on policies and procedures and offers a framework for pre-college personnel at MSU and other organizations to collaborate. The committee's purpose over the past several years has been to support and elevate these programs through dissemination of best practices, sharing of resources, and professional development.
  </p>

  
</div>