<?php

$page_title='Places to Visit <i class="icon" aria-hidden="true" style="position: relative; top: -1px; margin-right: 4px; display: inline-block; rotate: 2deg;">
<svg xmlns="http://www.w3.org/2000/svg" width="46" height="36" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M429.6 92.1c4.9-11.9 2.1-25.6-7-34.7s-22.8-11.9-34.7-7l-352 144c-14.2 5.8-22.2 20.8-19.3 35.8s16.1 25.8 31.4 25.8H224V432c0 15.3 10.8 28.4 25.8 31.4s30-5.1 35.8-19.3l144-352z"/></svg>
</i>';


$header_content = '
<div class="col-12 col-md-9 me-md-auto">
    <p>
        Browse venues that offer learning opportunities for <span class="text-nowrap">pre-kindergarten</span> to high school learners.
    </p>
</div>

'.$filter_btn_reversed.'

';

$include_filters = "true";
?>

<?php include("Views/Shared/Partials/view-controls.php"); ?>

<div id="resourceView" class="resource-view-normal">
    <section class="resource-data container">

            <?php
                error_reporting(E_ERROR | E_PARSE);
                
                $datafile = fopen("Content/SYPresources.csv", "r");
                
                if ($datafile !== FALSE) {
                    while (! feof($datafile)) {
                        $data = fgetcsv($datafile, 1000, ",");
                    
                        include("Views/Shared/Partials/data-map.php");
                        
                        if (! empty($data) && $status == "Active" && $place == "1") {
            ?>
            
                <?php include("Views/Shared/Partials/resource-item.php") ?>

            <?php
            }
            }
            }
            fclose($datafile);
            ?>

    </section>
</div>