<?php $page_title='
  Search by Grade
  
    <i class="icon" aria-hidden="true" style="position: relative; top: -5px; margin-right: 4px; display: inline-block;">
    <svg xmlns="http://www.w3.org/2000/svg" width="54" height="36" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path fill="currentColor" d="M337.8 5.4C327-1.8 313-1.8 302.2 5.4L166.3 96H48C21.5 96 0 117.5 0 144V464c0 26.5 21.5 48 48 48H256V416c0-35.3 28.7-64 64-64s64 28.7 64 64v96H592c26.5 0 48-21.5 48-48V144c0-26.5-21.5-48-48-48H473.7L337.8 5.4zM96 192h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V208c0-8.8 7.2-16 16-16zm400 16c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H512c-8.8 0-16-7.2-16-16V208zM96 320h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H96c-8.8 0-16-7.2-16-16V336c0-8.8 7.2-16 16-16zm400 16c0-8.8 7.2-16 16-16h32c8.8 0 16 7.2 16 16v64c0 8.8-7.2 16-16 16H512c-8.8 0-16-7.2-16-16V336zM232 176a88 88 0 1 1 176 0 88 88 0 1 1 -176 0zm88-48c-8.8 0-16 7.2-16 16v32c0 8.8 7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H336V144c0-8.8-7.2-16-16-16z"/></svg>
  </i>';

$header_content = '
<div class="col-12 col-md-9 me-md-auto">
    <p>
      Spartan Youth Programs displays programs available to serve age ranges from pre-kindergarten to high school. Select the grades you are intereseted in seeing programs for and then click the search button.
    </p>
</div>

'.$filter_btn_reversed.'

';

$include_filters = "true";
?>


<div class="col-12">
  <section>
    <div class="container">
      <div class="row">
      
        <div class="catalog-view-search p-0">      
          <span>            
            <ol class="tags tags-grades row">

              <li class="tag col-12 col-md-4">
                  <input type="checkbox" class="btn-check" id="all-grades" autocomplete="off" onclick="toggle(this);"></input>
                  <label class="btn btn-outline-tag-grade h-100" for="all-grades"> All Grades </label>
              </li>

              <?php
                

                foreach ($grades as $grade) {
                  echo '
                    <li class="tag col-6 col-md-4">
                      <input type="checkbox" class="btn-check" id="'.$grade.'-search" autocomplete="off"></input>
                      <label class="btn btn-outline-tag-grade h-100" for="'.$grade.'-search"> '.$grade.' </label>
                    </li>
                  ';
                }
              ?>

            </ol>
          </span>
        </div>
      </div>
    </div>  
  </section>

        

  <section class="container">
    <div class="row">
      <div class="col-12 col-md-4 p-0 ps-md-0 pe-md-3 mb-5">
        
        <a href="search" class="btn btn-theme btn-theme-syp-link d-block">
          Search
        </a>

      </div>
    </div>  
  </section>
</div>


<script>
          function toggle(source) {
            var checkboxes = document.querySelectorAll('input[type="checkbox"]');
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] != source)
                    checkboxes[i].checked = source.checked;
            }
          }
        </script>