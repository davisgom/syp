<section class="homepage-intro">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-7">
        <h2 class="intro-text">
          Programs and Activities for <br class="d-none d-lg-inline" />
          <span class="text-nowrap">Pre-K</span> through 12<sup style="font-size: .6em;">th</sup> Graders
        </h2>

        <br />

        <p>
          Discover a wide range of exciting opportunities for youth to improve their knowledge and skills in specific subject areas.
        </p>

        <!-- <a href="search" class="btn btn-theme btn-theme-syp-link btn-theme-small m-1">
          Browse All Programs
        </a> -->
      </div>

      <div class="col-11 col-md-auto order-md-3 program-match">
        <?php include("Views/Shared/Partials/program-match.php"); ?>
      </div>

      <div class="col-12 col-md-5 intro-image">
        <img src="Content/Images/syp-homepage-intro-<?php echo(rand(1,3)); ?>.png" class="img-fluid" />
      </div>

    </div>
  </div>
</section>

<section class="homepage-search-tiles">
  <?php include("Views/Shared/Partials/search-tiles-3.php") ?>  
</section>


<section class="homepage-dualenrollment">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-5 col-lg-3 de-image">
        <!-- empty div for background image -->
      </div>

      <div class="col-12 col-md-7 col-lg-7 de-content">
        <h2>
          Looking for College Courses <br class="d-none d-lg-inline" />
          for High School Students?
        </h2>
        <p>
          Dual Enrollment is an opportunity for students in grades 9.5-12 to enroll in select undergraduate courses at Michigan State University while in high school.
        </p>

        <a href="https://dualenrollment.msu.edu" target="_blank" class="btn btn-theme btn-theme-outline btn-theme-outline-reversed btn-theme-small">Learn More</a>
      </div>
    </div>
  </div>
</section>

<section class="homepage-about">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8 about-content">
        <h2>What is SYP?</h2>
        <p>
          Michigan State University's Spartan Youth Programs Web site displays a wide range of exciting opportunities for youth to improve their knowledge and skills in specific subject areas. Programs are available to serve all age ranges from pre-kindergarten to high school.
        </p>

        <a href="about" class="btn btn-theme btn-theme-outline btn-theme-outline-primary btn-theme-small">Learn More</a>
      </div>
    </div>
  </div>
</section>