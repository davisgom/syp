<?php

$page_title="Search Results";

$header_content = '
<div class="col-12 col-md-9 me-md-auto">
    <p>
        Browse programs offered during the summer for <span class="text-nowrap">pre-kindergarten</span> to high school learners.
    </p>
</div>

'.$filter_btn.'

';

$include_filters = "true";
?>

<?php include("Views/Shared/Partials/view-controls.php"); ?>

<div id="resourceView" class="resource-view-normal">
    <section class="resource-data container">
        <header class="program-section-header">
            <h2 class="text-syp-blue">Programs, Camps, and Activities</h2>
        </header>

        <?php
            error_reporting(E_ERROR | E_PARSE);
            
            $datafile = fopen("Content/SYPresources.csv", "r");

            if ($datafile !== FALSE) {
                while (! feof($datafile)) {
                    $data = fgetcsv($datafile, 1000, ",");
                
                    include("Views/Shared/Partials/data-map.php");
                        
                    if (! empty($data) && $status == "Active" && $program == "1") {
        ?>
        
            <?php include("Views/Shared/Partials/resource-item.php") ?>

        <?php
        }
        }
        }
        fclose($datafile);

        $datafile = fopen("Content/SYPresources.csv", "r");
        ?>
                    
    </section>

    <hr class="divider" />

    <section class="resource-data container">
        <header class="places-section-header">
            <h2>Places to Visit</h2>
        </header>

        <?php
            if ($datafile !== FALSE) {
                while (! feof($datafile)) {
                    $data = fgetcsv($datafile, 1000, ",");
                
                    include("Views/Shared/Partials/data-map.php");
                        
                    if (! empty($data) && $status == "Active" && $place == "1") {
        ?>
        
            <?php include("Views/Shared/Partials/resource-item.php") ?>

        <?php
        }
        }
        }
        fclose($datafile);
        ?>
            
    </section>
</div>